import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import bootstrap from './scss/bootstrap-loader.scss';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#e4002b',
    },
    secondary: {
      main: '#797979',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
    grey: {
      main: '#707070'
    }
  },
  bootstrap: bootstrap
});

export default theme;
