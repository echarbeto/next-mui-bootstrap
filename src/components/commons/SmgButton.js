import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PropTypes from "prop-types";
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(5),
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    fontWeight: 500,
    padding: '7px 15px',
    '&:hover': {
      color: '#fff',
    },
  },
  rounded: {
    borderRadius: '30px'
  },
  bootstrapButton: {
    margin: theme.spacing(5),
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    fontWeight: 500,
    backgroundColor: `${theme.palette.primary.main} !important`,
    color: '#fff',
    height: '25px',
    borderRadius: '2px',
    border: '0px',
    padding: '5px 10px',
    '&:hover': {
      textDecoration: 'underline',
      color: '#fff',
    },
  }
}));

const SmgButton = (
  {
    href,
    onClick,
    link,
    label,
    color,
    className,
    outline,
    secondary,
    customStyles,
    rounded,
    variant,
    bootstrap
  }
) => {
  const classes = useStyles();
  const classNames = [
    bootstrap ? classes.bootstrapButton : classes.button,
    rounded ? classes.rounded : ''
  ];
  return (
    <Button variant={variant} color={ color } href={href} className={classNames.join(' ')}>
      { label }
    </Button>
  );
};

SmgButton.propTypes = {
  label: PropTypes.string.isRequired,
  href: PropTypes.string,
  onClick: PropTypes.func,
  customStyles: PropTypes.object,
  variant: PropTypes.string,
  rounded: PropTypes.bool,
  className: PropTypes.string,
  bootstrap: PropTypes.bool
};

SmgButton.defaultProps = {
  href: '',
  customStyles: {},
  onClick: () => {},
  variant: 'contained',
  className: '',
  rounded: false,
  bootstrap: false
};

export default SmgButton;
